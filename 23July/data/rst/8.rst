
.. image:: /home/w77/prmvchk/pics/8.jpeg

Outnumbered by twenty to one in perilous weather conditions, Subedar Joginder
Singh’s platoon of 30 soldiers repealed two waves of enemies (200 in each) and
despite being reduced to half, once again set themselves up. Lacking everything
but spirit, Subedar Singh and his handful of men fixed bayonets on their rifles and
wreaked havoc on Chinese with their war cry everywhere in air: ‘Jo Bole So Nihal,
Sat Sri Akal’. With obvious superiority in strength, the enemy finally managed to
capture the nearly-dead hero of the Indian Army who took his last breath as a
Chinese POW.