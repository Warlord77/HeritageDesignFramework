
.. image:: /home/w77/prmvchk/pics/19.jpeg

Then 19-year-old, Subedar Yadav was one among the seven commandos of ‘Ghatak’
platoon who scaled up the wild Tiger Hill standing at a frightening height of 5300-
meter, beating all odds in between. Clearing their ways, he and six others captured
an enemy post before killing 10 Pakistanis and injuring 2 others with their limited
stock of ammunition. However, sitting on an advantage position, when Pakistanis
attacked with Rocket-Propelled Grenades (RPGs) and Uber Machine Guns (UMGs),
all of his comrades fell down, leaving behind only Subedar Yadav to fight till his last
breath. His Brothers in Arms were lying dead beside him before gunning down 35
Pakistani soldiers in straight combat. Despite he was down with 15 bullet injuries
with legs and face already damaged in grenade attacks, he blew away the head of
an enemy by lobbing grenade, shot down 4, and shooed away others by opening
machine gun fire. Crawling his way down with nearly-dead body, he reached alive
to his camp and informed his commander about the enemy. His exceptional
courage and crucial information is one of the answers to how Indian Army won the
Battle of Kargil.