'''
MainScreen

 ==============================================
|        \ Digital Wall of heroes /            |
|          ----------------------              |
|     ___     ___     ___     ___     ___      |
|    [___]   [___]   [___]   [___]   [___]     |
|                                              |
|     ___     ___     ___     ___     ___      |
|    [___]   [___]   [___]   [___]   [___]     |
|                                              |
|     ___     ___     ___     ___     ___      |
|    [___]   [___]   [___]   [___]   [___]     |
|           _____________________              |
|         /  B    >  T    K   o   \            |
 ==============================================


'''

from kivy.factory import Factory
from kivy.lang import Builder

class CoverImage(Factory.CoverBehavior, Factory.Image):
    """Image using cover behavior.
    """

    def __init__(self, **kwargs):
        super(CoverImage, self).__init__(**kwargs)

    def on_texture(self, instance, texture):
        self.reference_size = texture.size


from kivy.properties import StringProperty
class Picture(Factory.Widget):
    '''Picture is the class that will show the image with a white border and a
    shadow. They are nothing here because almost everything is inside the
    picture.kv. Check the rule named <Picture> inside the file, and you'll see
    how the Picture() is really constructed and used.

    The source property will be the filename to show.
    '''

    name = StringProperty('')

    source = StringProperty(None)

    Builder.load_string('''
<ShadowLabel@Label>
    canvas.before:
        Color
            rgba: 0, 0, 0, .5
        Rectangle
            source: 'data/images/shadow.png'
            size: self.size
            pos: self.pos
    text_size: self.width, None
    halign: 'center'
    valign: 'middle'
    color: 1, 1, 1, 1


<Picture>:
    # each time a picture is created, the image can delay the loading
    # as soon as the image is loaded, ensure that the center is changed
    # to the center of the screen.
    size_hint: None, None
    size: image.size
    #on_release: app.load_screen(self.source)
    orientation: 'vertical'
    Image:
        id: image
        source: root.source
        pos: root.pos
        # create initial image to be 400 pixels width
        size: dp(200), dp(200) / self.image_ratio

        # add shadow background
        canvas.before:
            Color:
                rgba: 1,1,1,1
            BorderImage:
                source: 'data/images/shadow32.png'
                border: (36,36,36,36)
                size:(self.width+72, self.height+72)
                pos: (root.x - 36, root.y - 36)
    ShadowLabel:
        bold: True
        pos: root.x , root.y 
        text: root.name
        size_hint: None, None
        width: root.width
        font_size: sp(21)
''')


class MainMenu(Factory.Screen):
    '''
    '''

    Builder.load_string('''

<MainMenu>
    name: 'MainMenu'
    CoverImage:
        source: 'data/images/background.jpg'
        allow_stretch: True
        keep_ratio: False
    BoxLayout
        orientation: 'vertical'
        spacing: dp(19)
        padding: dp(10)
        ShadowLabel
            text: 'Digital Wall of Heroes'
            font_size: dp(54)
            size_hint_y: None
            height: dp(64)
        GridLayout
            id: grid
            cols: 7
            padding: dp(19)
            spacing: dp(30)

''')

    def on_enter(self):
        import os
        dirs = [dr for dr in os.listdir(os.path.abspath('./data/heroes')) if not dr.startswith('.')]
        for dr in dirs:
            pic = Picture(source='data/heroes/' + dr + '/' + dr + '.jpg', name=dr)
            self.ids.grid.add_widget(pic)